/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.javaswing;

import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author EAK
 */
public class ButtonExample {
    public static void main(String[] args) {
        JFrame f = new JFrame("Button Example");
        JButton b = new JButton("Click Here");
        b.setBounds(50, 100, 95, 35);
        
        final JTextField tf = new JTextField();
        tf.setBounds(50, 50, 150, 20);
        
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tf.setText("Welcom to Javatpoint.");
            }
        });
        
        JButton b2 = new JButton(new ImageIcon("D:\\Click.png"));
        b2.setBounds(100, 150, 150, 50);
        
        
        f.add(b2);
        f.add(b);
        f.add(tf);
        f.setSize(400, 400);
        f.setLayout(null);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }
}
