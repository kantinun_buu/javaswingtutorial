/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.javaswing;

import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;

/**
 *
 * @author EAK
 */
public class JToolBarExample {

    public static void main(String[] args) {
        JFrame f = new JFrame("JToolBar");
        f.setDefaultCloseOperation(3);
        JToolBar toolbar = new JToolBar();
        toolbar.setRollover(true);
        JButton b = new JButton("File");
        toolbar.add(b);
        toolbar.addSeparator();
        toolbar.add(new JButton("Edit"));
        toolbar.add(new JComboBox(new String[]{"Opt-1", "Opt-2", "Opt-3", "Opt-4"}));
        Container contentPane = f.getContentPane();
        contentPane.add(toolbar, BorderLayout.NORTH);
        JTextArea ta = new JTextArea();
        JScrollPane mypane = new JScrollPane(ta);
        contentPane.add(mypane, BorderLayout.EAST);
        f.setSize(450, 250);
        f.setVisible(true);
    }
}
