/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.javaswing;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author EAK
 */
public class PanelExample {

    public PanelExample() {
        final JFrame f = new JFrame();
        JPanel panel = new JPanel();
        panel.setBounds(40, 80, 200, 200);
        panel.setBackground(Color.CYAN);
        JButton b1 = new JButton("Button 1");
        b1.setBounds(50, 100, 80, 30);
        b1.setBackground(Color.MAGENTA);
        JButton b2 = new JButton("Button 2");
        b2.setBounds(100, 100, 80, 30);
        b2.setBackground(Color.ORANGE);
        b1.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(f, "Button 1 pressed.");
            }
        });
        b2.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(f, "Button 2 pressed");
            }
            
        });
        panel.add(b1);
        panel.add(b2);
        f.add(panel);
        f.setSize(400, 400);
        f.setLayout(null);
        f.setDefaultCloseOperation(3);
        f.setVisible(true);
    }
    
    public static void main(String[] args) {
        new PanelExample();
    }
    
}
