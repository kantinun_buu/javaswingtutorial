package com.kantinun.javaswing;


import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JComponent;
import javax.swing.JFrame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author EAK
 */

class MyJComponent extends JComponent{
    public void paint(Graphics g){
        g.setColor(Color.green);
        g.fillRect(30, 30, 100, 100);
    }
}

public class JComponentExample {
    public static void main(String[] args) {
        MyJComponent com = new MyJComponent();
        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("JComponent Example");
        frame.setSize(300, 200);
        frame.setDefaultCloseOperation(3);
        frame.add(com);
        frame.setVisible(true);
    }
}
