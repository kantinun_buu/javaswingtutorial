/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.javaswing;

import javax.swing.*;
import java.awt.*;

/**
 *
 * @author EAK
 */
public class IconExample {

    public IconExample() {
        JFrame f = new JFrame();
        Image icon = Toolkit.getDefaultToolkit().getImage("D:\\minecraft_logo_icon.png");
        f.setIconImage(icon);
        f.setLayout(null);
        f.setSize(200, 200);
        f.setDefaultCloseOperation(3);
        f.setVisible(true);
    }
    
    public static void main(String[] args) {
        new IconExample();
    }
    
}
