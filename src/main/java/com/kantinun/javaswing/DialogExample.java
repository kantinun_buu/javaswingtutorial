/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.javaswing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 *
 * @author EAK
 */
public class DialogExample {

    private static JDialog d;

    public DialogExample() {
        JFrame f = new JFrame("Frame");
        JButton b = new JButton("Open Dialog");
        b.setBounds(50, 50, 150, 50);
        d = new JDialog(f, "Dialog Example", true);
        d.setLayout(new FlowLayout());
        JButton b2 = new JButton("OK");
        
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DialogExample.d.setVisible(true);
            }

        });
        
        b2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DialogExample.d.setVisible(false);
            }
        });
        
        d.add(new JLabel("Click button to continue."));
        d.add(b2);
        d.setSize(300, 300);
        f.add(b);
        f.setSize(300, 300);
        f.setLayout(null);
        f.setDefaultCloseOperation(3);
        f.setVisible(true);
        
    }

    public static void main(String args[]) {
        new DialogExample();
    }
}
