/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.javaswing;

import java.awt.*;
import javax.swing.*;

/**
 *
 * @author EAK
 */
public class SeparatorExample {

    JMenu menu, submenu;
    JMenuItem i1, i2, i3, i4, i5;

    public SeparatorExample() {
        JFrame f = new JFrame("Separator Example");
        JMenuBar mb = new JMenuBar();
        menu = new JMenu("Menu");
        i1 = new JMenuItem("Item 1");
        i2 = new JMenuItem("Item 2");
        menu.add(i1);
        menu.addSeparator();
        menu.add(i2);
        mb.add(menu);

        JLabel l1 = new JLabel("Above Separator");
        JLabel l2 = new JLabel("Below Separator");
        JSeparator sep = new JSeparator();
        f.add(l1);
        f.add(sep);
        f.add(l2);
        f.setJMenuBar(mb);
        f.setSize(400, 400);
        f.setLayout(new GridLayout(0, 1));
        f.setDefaultCloseOperation(3);
        f.setVisible(true);
    }

    public static void main(String args[]) {
        new SeparatorExample();
    }
}
