/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.javaswing;

import javax.swing.JEditorPane;
import javax.swing.JFrame;

/**
 *
 * @author EAK
 */
public class JEditorPaneExample {

    JFrame myFrame = null;

    public static void main(String[] a) {
        (new JEditorPaneExample()).test();
    }

    private void test() {
        myFrame = new JFrame("JEditorPane Test");
        myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myFrame.setSize(400, 200);
        JEditorPane myPane = new JEditorPane();
        myPane.setContentType("text/html");
        myPane.setText("<h1>Test</h1> <p>Message<p>");
        myFrame.setContentPane(myPane);
        myFrame.setVisible(true);
    }
}
