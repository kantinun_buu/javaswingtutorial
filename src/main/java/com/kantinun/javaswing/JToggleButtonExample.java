/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.javaswing;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author EAK
 */
public class JToggleButtonExample extends JFrame implements ItemListener {

    private JToggleButton button;

    public JToggleButtonExample() {
        setTitle("JToggleButton with ItemListener Example");
        setLayout(new FlowLayout());
        setJToggleButton();
        setAction();
        setSize(200, 200);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void setJToggleButton() {
        button = new JToggleButton("ON");
        add(button);
    }

    private void setAction() {
        button.addItemListener(this);
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if(button.isSelected())
            button.setText("OFF");
        else
            button.setText("ON");
    }

    public static void main(String[] args) {
        new JToggleButtonExample();
    }
}
