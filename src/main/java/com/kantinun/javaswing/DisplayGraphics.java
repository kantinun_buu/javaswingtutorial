/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.javaswing;

import java.awt.*;
import javax.swing.JFrame;

/**
 *
 * @author EAK
 */
public class DisplayGraphics extends Canvas {

    @Override
    public void paint(Graphics g) {
        g.drawString("Hello", 40, 40);
        setBackground(Color.WHITE);
        g.fillRect(130, 30, 100, 80);
        g.drawOval(30, 130, 50, 60);
        setForeground(Color.BLUE);
        g.fillOval(130, 130, 50, 60);
        g.drawArc(30, 200, 40, 50, 90, 60);
        g.fillArc(30, 130, 40, 50, 180, 40);
        
        Toolkit t = Toolkit.getDefaultToolkit();
        Image i = t.getImage("D:\\moving_picture.gif");
        g.drawImage(i, 100, 200, 200, 100, this);
    }
    
    public static void main(String[] args) {
        DisplayGraphics m = new DisplayGraphics();
        JFrame f = new JFrame("Display Graphics");
        f.add(m);
        f.setSize(400, 400);
        f.setDefaultCloseOperation(3);
        f.setVisible(true);
    }
}
