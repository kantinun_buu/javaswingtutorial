/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.javaswing;

import javax.swing.*;

/**
 *
 * @author EAK
 */
public class TabbedPaneExample {

    JFrame f;

    public TabbedPaneExample() {
        f = new JFrame();
        JTextArea ta = new JTextArea(200, 200);
        JPanel p1 = new JPanel();
        p1.add(ta);
        JPanel p2 = new JPanel();
        JPanel p3 = new JPanel();
        JTabbedPane tp = new JTabbedPane();
        tp.setBounds(50, 50, 200, 200);
        tp.add("One", p1);
        tp.add("Two", p2);
        tp.add("Three", p3);
        f.add(tp);
        f.setSize(400, 400);
        f.setLayout(null);
        f.setDefaultCloseOperation(3);
        f.setVisible(true);
    }

    public static void main(String[] args) {
        new TabbedPaneExample();

    }

}
