/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.javaswing;

import javax.swing.*;
import javax.swing.event.*;

/**
 *
 * @author EAK
 */
public class SpinnerExample {

    public static void main(String[] args) {
        JFrame f = new JFrame("Spinner Example");
        final JLabel label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setSize(250, 100);
        SpinnerModel value = new SpinnerNumberModel(5, 0, 10, 1);
        JSpinner spinner = new JSpinner(value);
        spinner.setBounds(100, 100, 50, 30);
        f.add(spinner);
        f.add(label);
        f.setSize(300, 300);
        f.setLayout(null);
        f.setDefaultCloseOperation(3);
        f.setVisible(true);
        spinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                label.setText("Value: " + ((JSpinner) e.getSource()).getValue());
            }
        });

    }
}
