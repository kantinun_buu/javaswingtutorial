/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.javaswing;

import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author EAK
 */
public class CheckBoxExample {

    CheckBoxExample() {
        JFrame f = new JFrame("CheckBox Example");
        final JLabel label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setSize(400, 100);
        JCheckBox checkBox1 = new JCheckBox("C++");
        checkBox1.setBounds(150, 100, 50, 50);
        JCheckBox checkBox2 = new JCheckBox("Java");
        checkBox2.setBounds(150, 150, 50, 50);
        f.add(checkBox1);
        f.add(checkBox2);
        f.add(label);
        checkBox1.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                label.setText("C++ Checkbox:"
                        + (e.getStateChange() == 1 ? "checked" : "unchecked"));
            }
        });
        checkBox2.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                label.setText("Java Checkbox:"
                        + (e.getStateChange() == 1 ? "checked" : "unchecked"));
            }
        });

        f.setSize(400, 400);
        f.setLayout(null);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }

    public static void main(String args[]) {
        new CheckBoxExample();
    }
}
