/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.javaswing;

import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author EAK
 */
public class PopupMenuExample {

    public PopupMenuExample() {
        final JFrame f = new JFrame("PopupMenu Example");
        final JPopupMenu popupmenu = new JPopupMenu("Edit");
        final JLabel label = new JLabel("", JLabel.CENTER);
        label.setSize(400, 100);
        JMenuItem cut = new JMenuItem("Cut");
        JMenuItem copy = new JMenuItem("Copy");
        JMenuItem paste = new JMenuItem("Paste");
        popupmenu.add(cut);
        popupmenu.add(copy);
        popupmenu.add(paste);
        f.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                popupmenu.show(f, e.getX(), e.getY());
            }
        });
        cut.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                label.setText("Cutted.");
            }
        });
        copy.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                label.setText("Copied.");
            }
        });
        paste.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                label.setText("Pasted.");
            }
        });
        f.add(label);
        f.add(popupmenu);
        f.setSize(400, 400);
        f.setLayout(null);
        f.setDefaultCloseOperation(3);
        f.setVisible(true);
    }

    public static void main(String args[]) {
        new PopupMenuExample();
    }
}
