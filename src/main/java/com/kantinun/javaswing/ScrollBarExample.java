/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.javaswing;

import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import javax.swing.*;

/**
 *
 * @author EAK
 */
public class ScrollBarExample {

    public ScrollBarExample() {
        JFrame f = new JFrame("Scrollbar Example");
        final JScrollBar s = new JScrollBar();
        s.setBounds(100, 100, 50, 100);
        final JLabel label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setSize(400, 100);
        f.add(s);
        f.add(label);
        f.setSize(400, 400);
        f.setLayout(null);
        f.setDefaultCloseOperation(3);
        f.setVisible(true);
        
        s.addAdjustmentListener(new AdjustmentListener(){
            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                label.setText("Scroll bar value : " + s.getValue());
            }
            
        });
    }

    public static void main(String[] args) {
        new ScrollBarExample();
    }
}
